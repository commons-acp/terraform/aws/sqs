
output "lambda_api_arn" {
  value = module.lambda.lambda_api_arn
}
output "lambda_api_invoke_arn" {
  value = module.lambda.lambda_api_invoke_arn
}
output "lambda_api_alias" {
  value = module.lambda.lambda_api_alias
}
output "lambda_api_function_name" {
  value = module.lambda.lambda_api_function_name
}

output "arn" {
  value       = aws_sqs_queue.queue.arn
  description = "The ARN for the created Amazon SQS queue"
}
output "id" {
  value       = aws_sqs_queue.queue.id
  description = "The URL for the created Amazon SQS queue"
}
output "name" {
  value       = aws_sqs_queue.queue.name
  description = "The NAME for the created Amazon SQS queue"
}

