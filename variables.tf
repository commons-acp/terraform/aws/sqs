
variable "lambda_role_arn" {
  description = ""
}
variable "function_name" {
  description = ""
}
variable "gitlab_package_url" {
  description = ""
}
variable "gitlab_api_token" {
  description = ""
}
variable "file_name" {
  description = ""
}
variable "lambda_env" {
  description = ""
}

variable "name" {
  type        = string
  description = "The SQS queue name"
}
variable "visibility_timeout_seconds" {
  type        = number
  description = " souled be more then 50 if you using sqs que with lambda function ,The visibility timeout for the queue. An integer from 0 to 43200 (12 hours). The default for this attribute is 30."
  default     = 50
}
variable "delay_seconds" {
  type        = number
  description = "The time in seconds that the delivery of all messages in the queue will be delayed."
  default     = 0
}
variable "max_message_size" {
  type        = number
  description = "The limit of how many bytes a message can contain before Amazon SQS rejects it. An integer from 1024 bytes (1 KiB) up to 262144 bytes (256 KiB). The default for this attribute is 262144 (256 KiB)"
  default     = 25000
}
variable "message_retention_seconds" {
  type        = number
  description = "The number of seconds Amazon SQS retains a message. Integer representing seconds, from 60 (1 minute) to 1209600 (14 days). The default for this attribute is 345600 (4 days)"
  default     = 345600
}
variable "receive_wait_time_seconds" {
  type        = number
  description = "The time for which a ReceiveMessage call will wait for a message to arrive (long polling) before returning. An integer from 0 to 20 (seconds). The default for this attribute is 0, meaning that the call will return immediately"
  default     = 0
}
variable "max_receive_count" {
  type        = number
  description = "maxReceiveCount for the Dead Letter Queue redrive policy"
  default     = 5
}

variable "node_version" {
  description = "node version"
  default     = "nodejs14.x"
}


