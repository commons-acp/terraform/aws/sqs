

resource "aws_sqs_queue" "queue" {
  depends_on                 = [module.lambda]
  name                       = var.name
  visibility_timeout_seconds = var.visibility_timeout_seconds
  delay_seconds              = var.delay_seconds
  receive_wait_time_seconds  = var.receive_wait_time_seconds
  message_retention_seconds  = var.message_retention_seconds
  max_message_size           = var.max_message_size
}

resource "aws_sqs_queue_policy" "access-policy" {
  depends_on = [data.aws_caller_identity.current, module.lambda]
  queue_url  = aws_sqs_queue.queue.id
  policy     = <<POLICY
{
  "Version": "2008-10-17",
  "Id": "__default_policy_ID",
  "Statement": [
    {
      "Sid": "__owner_statement",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
      },
      "Action": "SQS:*",
      "Resource": "arn:aws:sqs:eu-west-1:${data.aws_caller_identity.current.account_id}:${var.name}"
    }
  ]
}
POLICY
}

data "aws_caller_identity" "current" {}

# Event source from SQS
resource "aws_lambda_event_source_mapping" "event_source_mapping" {
  depends_on       = [module.lambda]
  event_source_arn = aws_sqs_queue.queue.arn
  enabled          = true
  function_name    = var.function_name
  batch_size       = 1
}
