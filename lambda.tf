module "lambda" {
  depends_on         = [data.aws_caller_identity.current]
  source             = "git::https://git@gitlab.com/commons-acp/terraform/aws/lambda?ref=v1.4.0"
  lambda_role_arn    = var.lambda_role_arn
  function_name      = var.function_name
  gitlab_package_url = var.gitlab_package_url
  gitlab_api_token   = var.gitlab_api_token
  lambda_env         = var.lambda_env
  runtime            = var.node_version
  lambda_file_path   = "${abspath(path.root)}/${var.file_name}"
  providers = {
    aws = aws
  }
}
